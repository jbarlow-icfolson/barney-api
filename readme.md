#Barney API


##Requirements
 - [Node 5.4.1](https://nodejs.org/download/release/v5.4.1/)
 - [NPM 3.3.12](https://www.npmjs.com/)


##Setup
 - Repo Link: `https://jbarlow-icfolson@bitbucket.org/jbarlow-icfolson/barney-api.git`
 - Run `npm start`


##Description
 - This is the CRUD api for the barney bot database.