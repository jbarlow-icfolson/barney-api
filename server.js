'use strict';

const express = require('express'),
      bodyParser = require('body-parser'),
      mongodb = require('mongodb'),
      http = require('http'),
      app = express(),
      CONSUMPTION_COLLECTION = 'consumption',
      ObjectID = mongodb.ObjectID;


// generate connection string based on running locally versus running in azure
var connectionString;
try {
  const appConfig = require('./app.config.json');
  connectionString = appConfig.DOCDBCONNSTR_BARNEY_DB;
} catch (ex) {
  connectionString = process.env.DOCDBCONNSTR_BARNEY_DB;
}

// App Settings
app.use(bodyParser.json());

// Start Server
app.listen(3000, () => {
  const server = app.listen(process.env.PORT || 8080, () => {
    const port = server.address().port;
    console.log("App now running on port ", port);
  });
});

/*    Routing
 *    '/'
 *    single page app entry point
 */

app.get('/', (req, res) => {
  res.status(200).send('Nothing to see here.');
});


/*    CRUD Commands
 *    handleErr: generic error handling
 *
 *    '/daily-consumption'
 *    GET: returns all consumption records
 *    PUT: inserts single consumption record
 *
 *    '/daily-consumption/:id'
 *    GET: find consumption record by ID
 *    PUT: updates single consumption record by ID
 *
 *    '/daily-consumption/by-date/:date'
 *    GET: find all consumption records by date
 */

const handleErr = options => {
  /*    Paramters
  *
  *     res: server response
  *     reason: reason for error
  *     message: error message
  *     code: error code
  */

  console.error("reason: ", options.reason);
  console.error("code: ", options.code);

  options.res.status(options.code || 500).json({"error": options.message});
};

app.get('/daily-consumption', (req, res) => {
  mongodb.MongoClient.connect(connectionString, (err, db) => {
    if (err) {
      handleErr({
        'res': res,
        'reason': err, 
        'message': 'Failed to connect to the database.'
      });
    } else {
      db.collection(CONSUMPTION_COLLECTION).find().toArray((err, result) => {
        if (err) {
          handleErr({
            'res': res,
            'reason': err, 
            'message': 'Failed to get the collection from the database.'
          });
        } else {
          res.status(200).json(result);
        }
      });

      db.close();
    }
  });
});

app.post('/daily-consumption', (req, res) => {
  mongodb.MongoClient.connect(connectionString, (err, db) => {
    if (err) {
      handleErr({
        'res': res,
        'reason': err, 
        'message': 'Failed to connect to the database.'
      });
    } else {
      const newKeg = req.body;

      db.collection(CONSUMPTION_COLLECTION).insertOne(newKeg, function(err, result) {
        if (err) {
          handleErr({
            'res': res,
            'reason': err, 
            'message': 'Failed to update the database.'
          });
        } else {
          res.status(201).json(result.ops[0]);
        }

        db.close();
      });
    }
  });
});

app.get('/daily-consumption/:id', (req, res) => {
  const newObjID = req.params.id;

  if (ObjectID.isValid(newObjID)) {
    mongodb.MongoClient.connect(connectionString, (err, db) => {
      if (err) {
        handleErr({
          'res': res,
          'reason': err, 
          'message': 'Failed to connect to the database.'
        });
      } else {
        db.collection(CONSUMPTION_COLLECTION).findOne({ _id: new ObjectID(newObjID)}, (err, result) => {
          console.error(err, result);

          if (result === null) {
            handleErr({
              'code': 404,
              'res': res,
              'reason': err,
              'message': 'No match found.'
            });
          } else {
            res.status(200).json(result);
          }

          db.close();
        });
      }
    });
  } else {
    handleErr({
      'res': res,
      'reason': null, 
      'message': 'Invalid ID.'
    });
  }
});

app.post('/daily-consumption/:id', (req, res) => {
  const newKeg = req.body,
        newObjID = req.params.id;

  if (ObjectID.isValid(newObjID)) {
    mongodb.MongoClient.connect(connectionString, (err, db) => {
      if (err) {
        handleErr({
          'res': res,
          'reason': err, 
          'message': 'Failed to connect to the database.'
        });
      } else {
        db.collection(CONSUMPTION_COLLECTION).updateOne({_id: new ObjectID(newObjID)}, newKeg, function(err, result) {
          if (err) {
            handleErr(res, err.message, "Failed to update keg.");
          } else {
            res.status(200).send(newKeg);
          }

          db.close();
        });
      }
    });
  } else {
    handleErr({
      'res': res,
      'reason': null, 
      'message': 'Invalid ID.'
    });
  }
});

app.get('/daily-consumption/by-date/:date', (req, res) => {
  const newDate = req.params.date;

  if (newDate.length > 0) {
    mongodb.MongoClient.connect(connectionString, (err, db) => {
      if (err) {
        handleErr({
          'res': res,
          'reason': err, 
          'message': 'Failed to connect to the database.'
        });
      } else {
        db.collection(CONSUMPTION_COLLECTION).find({date: newDate}).toArray((err, result) => {
          if (result === null) {
            handleErr({
              'code': 404,
              'res': res,
              'reason': err,
              'message': 'No match found.'
            });
          } else {
            res.status(200).json(result);
          }

          db.close();
        });
      }
    });
  } else {
    handleErr({
      'res': res,
      'reason': null, 
      'message': 'Invalid Date.'
    });
  }
});